/**	\file	tableviewbuddy.cpp
 *	\brief	Класс-партнёр для табличного представления (Qt4).
 *	\author	Литкевич Юрий (lit-uriy@yandex.ru).
 *	Этот класс задуман для расширения возможностей табличного представления
 *	наиболее распространёнными функциями. Такими как копирование, форматирование таблицы и др.
 **********************************************
 *	EN: 
 *	Class-buddy for table view (Qt4).
 *	Author:	Yuriy Litkevich (lit-uriy@yandex.ru).
 *	This class is conceived for the extension of possibilities of a table view
 *	by the most used functions. Such as copying, formatting of the table view, etc.
 */
#include <QTableView>
#include <QtDebug>
#include <QAction>
#include <QApplication>
#include <QClipboard>

#include "tableviewbuddy.h"

#include <QDebug>

#if 0
    #define DEBUG qDebug()
#else
    #define DEBUG  QMessageLogger(0, 0, 0).noDebug()
#endif

inline void initMyResource(){ Q_INIT_RESOURCE(tableviewbuddy); }

TableViewBuddy::TableViewBuddy(QAbstractItemView *tv)
    : QObject(tv)
    , copyWithHeader(false)
{
	view = tv;
	// RU: !!!!!!!!! Меняет политику контекстного меню представления !!!!!!!!!!!
	// EN: !!!!!!!!! Changes context menu policy policy of the table view !!!!!!!!!!!
	view->setContextMenuPolicy(Qt::ActionsContextMenu);

	initMyResource();
    // копирование ячеек
	actionCopy = new QAction(this);
    actionCopy->setIcon (QIcon(":/TableViewBuddy/copy.png"));
	actionCopy->setText(tr("&Copy"));
	actionCopy->setShortcut(tr("Ctrl+C"));
	actionCopy->setStatusTip(tr("Copy selected cells"));
	connect(actionCopy, SIGNAL(triggered()),
			 this,		SLOT(slotCopy()));
	
	view->addAction(actionCopy);

    // копирование ячеек с заголовками столбцов
    actionCopyWithHeader = new QAction(this);
    actionCopyWithHeader->setIcon (QIcon(":/TableViewBuddy/copy.png"));
    actionCopyWithHeader->setText(tr("&Copy with header"));
    actionCopyWithHeader->setStatusTip(tr("Copy selected cells with their headers"));
    connect(actionCopyWithHeader, SIGNAL(triggered()),
             this,		SLOT(slotCopy()));

    view->addAction(actionCopyWithHeader);

    // Удаление строк
    actionDeleteRow = new QAction(this);
    actionDeleteRow->setIcon (QIcon(":/TableViewBuddy/remove.png"));
    actionDeleteRow->setText(tr("&Delete row(s)"));
//    actionDeleteRow->setShortcut(tr("Ctrl+C"));
    actionDeleteRow->setStatusTip(tr("Delete selected rows"));
    connect(actionDeleteRow, SIGNAL(triggered()),
            this, SLOT(deleteRow()));

    view->addAction(actionDeleteRow);

    // Добавление строк
    actionInsertRow = new QAction(this);
    actionInsertRow->setIcon (QIcon(":/TableViewBuddy/add.png"));
    actionInsertRow->setText(tr("&Insert row"));
//    actionInsertRow->setShortcut(tr("Ctrl+V"));
    actionInsertRow->setStatusTip(tr("Insert row before selected or end of table"));
    connect(actionInsertRow, SIGNAL(triggered()),
            this, SLOT(insertRow()));

    view->addAction(actionInsertRow);
}



QAction* TableViewBuddy::addAction(QString atext, QIcon aicon)
{
    QAction *act = new QAction(this);
    act->setIcon (aicon);
    act->setText(atext);
    userActions.append(act);
    view->addAction(act);

    return act;
}

void TableViewBuddy::addAction(QAction *act)
{
    userActions.append(act);
    view->addAction(act);
}

void TableViewBuddy::removeAction(QAction *act)
{
    userActions.removeOne(act);
    view->removeAction(act);
}

void TableViewBuddy::setInsertDisabled(bool dis)
{
    actionInsertRow->setDisabled(dis);
}

void TableViewBuddy::setDeleteDisabled(bool dis)
{
    actionDeleteRow->setDisabled(dis);
}

void TableViewBuddy::setCopyWithHeader(bool en)
{
    copyWithHeader = en;
}


void TableViewBuddy::slotCopy()
{
    QAction *a = qobject_cast<QAction*>(sender());
    if (a == actionCopyWithHeader)
        copyWithHeader = true;
    else
        copyWithHeader = false;
	QApplication::clipboard()->setText(copy());
}

void TableViewBuddy::deleteRow()
{
    DEBUG << "TableViewBuddy::deleteRow()";
    // Берём минимумы и максимумы
    const QItemSelection ranges = view->selectionModel()->selection();
    if (!ranges.count())
        return;

    QAbstractItemModel *model = view->model();
    QList<QPersistentModelIndex> indexes;

    foreach (QItemSelectionRange range, ranges) {
        int minrow = range.top();
        int maxrow = range.bottom();

        DEBUG << "\tminrow =" << minrow << "\tmaxrow =" << maxrow;
        // запомним постоянные индексы
        for (int i = minrow; i <= maxrow; ++i) {
            indexes << model->index(i,0);
        }
    }
    // удалим строки по постоянным индексам
    foreach (QPersistentModelIndex i, indexes) {
        int row = i.row();
        emit rowBeforeDelete(row);
//        view->hideRow(row);
        model->removeRow(row);
        DEBUG << "\tprocesed row" << row;
    }
    emit rowsDeleted();
}

void TableViewBuddy::insertRow()
{
    QAbstractItemModel *model = view->model();
    int row = 0;
    const QItemSelection ranges = view->selectionModel()->selection();
    if (ranges.count()){
        // используем только первую выделенную область
        const QItemSelectionRange range = ranges.at(0);
        row = range.top();// над выделенной
    }else{
        row = model->rowCount(); // в конце таблицы
    }
    model->insertRow(row);
    emit rowInserted(row);
}

QString TableViewBuddy::copy()
{

    int minRowCommon = -1;
    int minColumnCommon = -1;
    int maxRowCommon = -1;
    int maxColumnCommon = -1;

    QItemSelectionModel *sm = view->selectionModel();


    // Ищем минимумы и максимумы строк и столбцов вовсех поддиапазонах
    const QItemSelection ranges = sm->selection();
    foreach (QItemSelectionRange range, ranges) {
        int minrow = range.top();
        int mincolumn = range.left();
        int maxrow = range.bottom();
        int maxcolumn = range.right();

        if (minRowCommon < 0)
            minRowCommon = minrow;
        if (minrow < minRowCommon)
            minRowCommon = minrow;

        if (minColumnCommon < 0)
            minColumnCommon = mincolumn;
        if (mincolumn < minColumnCommon)
            minColumnCommon = mincolumn;

        if (maxrow > maxRowCommon)
            maxRowCommon = maxrow;

        if (maxcolumn > maxColumnCommon)
            maxColumnCommon = maxcolumn;
    }


    DEBUG << "Copy, FROM" << QString("(%1,%2)").arg(minRowCommon).arg(minColumnCommon)
             << "TO" << QString("(%1,%2)").arg(maxRowCommon).arg(maxColumnCommon);

    QList<int> selectedColumns;
    QString		str, hdr;
    int i, j;
    QModelIndex	index;

    QTableView *tv = qobject_cast<QTableView*>(view);

    // Само копирование
    for (i=minRowCommon; i <= maxRowCommon; ++i)
    {
        if (i>minRowCommon)
            str += "\n";

        for (j=minColumnCommon; j <= maxColumnCommon; ++j)
        {

            if ((tv) && (tv->isColumnHidden(j)))
                continue;

            if (j>minColumnCommon)
                str += "\t";

            index = view->model()->index(i, j, QModelIndex());

            if (sm->isSelected(index)){
                str += view->model()->data(index).toString();
                selectedColumns << j;
            }else {
                str += QString();
            }
        }
    }


    // копирование заголовков
    if (copyWithHeader){
        for (j=minColumnCommon; j <= maxColumnCommon; ++j)
        {
            if ((tv) && (tv->isColumnHidden(j)))
                continue;

            if (j>minColumnCommon)
                hdr += "\t";

            if (selectedColumns.contains(j))
                hdr += view->model()->headerData(j, Qt::Horizontal).toString().simplified();
            else
                hdr += QString();
        }
        hdr += "\n";
    }

    return hdr + str;
}

